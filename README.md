# Projet front-end : MignonPedia

Site réalisé dans le cadre de la formation **Développement Java / Cobol** à l'INTI. Il s'agit d'un jeu de piste sur le site d'une firme imaginaire: MignonPedia.

100% fait main : html, css et js entièrement écrits à la main.

Le site deployé est accessible à l'adresse suivante : https://pages.moqueur.chat/inti/etienne/mignonpedia

## Installation locale

Pour naviguer sur le site local, il faut configurer un accès local HTTPS afin de pouvoir utiliser les fonctionnalités de chiffrement des navigateurs. Voir par exemple [mkcert](https://github.com/FiloSottile/mkcert).

## Captures d'écran

![Page d'accueil](./assets/images/2024-01-04-09-49-38.png)

![Pages animaux](./assets/images/2024-01-04-10-11-51.png) 

![Site responsive](./assets/images/2024-01-04-10-18-10.png) 
