async function getAnimals(path) {
  const response = await fetch(path)
  const animals = await response.json()
  return animals
}

function getRandoms(a, n) {
  if (n > a.length) return a
  for (let i = a.length; i > 0; i--) {
    let j = Math.floor(Math.random() * i);
    [a[i - 1], a[j]] = [a[j], a[i - 1]]
  }
  return a.slice(0, n)
}

function animalCards(animals, parent, rootPath) {
  animals.forEach(animal => {
    const card = document.createElement("div")
    card.classList.add("animalCard")

    const link = document.createElement("a")
    link.href = rootPath + "animals/animal.html?animal=" + animal.id
    const title = document.createElement("h3")
    title.innerText = animal.nom
    const cardImage = document.createElement("img")
    cardImage.src = rootPath + "assets/" + animal.card
    cardImage.alt = animal.nom
    link.appendChild(title)
    link.appendChild(cardImage)

    const desc = document.createElement("p")
    desc.innerText = animal.shortDesc

    card.appendChild(link)
    card.appendChild(desc)
    parent.appendChild(card)
  })
}

function setInnerHTML(elm, html) {
  elm.innerHTML = html;
  
  Array.from(elm.querySelectorAll("script"))
    .forEach( oldScriptEl => {
      const newScriptEl = document.createElement("script");
      
      Array.from(oldScriptEl.attributes).forEach( attr => {
        newScriptEl.setAttribute(attr.name, attr.value) 
      });
      
      const scriptText = document.createTextNode(oldScriptEl.innerHTML);
      newScriptEl.appendChild(scriptText);
      
      oldScriptEl.parentNode.replaceChild(newScriptEl, oldScriptEl);
  });
}

function animalPage(animal, parent, rootPath) {
  const title = document.createElement("h1")
  title.innerText = animal.nom
  const scientificName = document.createElement("p")
  const scientificNameInner = document.createElement("i")
  scientificNameInner.innerText = animal.nomSc
  scientificName.appendChild(scientificNameInner)
  const cover = document.createElement("img")
  cover.src = rootPath + "assets/" + animal.cover
  cover.alt = animal.nom
  const desc = document.createElement("p")
  setInnerHTML(desc, animal.desc)

  parent.appendChild(title)
  parent.appendChild(scientificName)
  parent.appendChild(cover)
  parent.appendChild(desc)
}

function isFuzzyMatch(pattern, string) {
  if (!pattern) return true
  var j = 0
  pattern = pattern.toLowerCase()
  string = string.toLowerCase()
  j = string.indexOf(pattern.charAt(0));
  while (j != -1 && string && pattern) {
    pattern = pattern.substring(1)
    string = string.substring(j+1)
    j = string.indexOf(pattern.charAt(0));
  }
  return !pattern
}

function searchAnimal(animals, pattern, parent, rootPath) {
  animals.filter(animal => isFuzzyMatch(pattern, animal.nom)).forEach(animal => {
    const link = document.createElement("a")
    link.href = rootPath + "animals/animal.html?animal=" + animal.id
    const li = document.createElement("li")
    li.innerText = animal.nom
    link.appendChild(li)
    parent.appendChild(link)
  })
}
