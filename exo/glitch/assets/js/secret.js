async function importKey(rootPath) {
  const jwk = await fetch(rootPath + "glitch/assets/key.json").then(r => r.json())
  const key = await window.crypto.subtle.importKey('jwk', jwk, 'aes-gcm', true, ['encrypt', 'decrypt'])
  return key
}

function uint8ToString(uint8Array) {
  return (Array.from(uint8Array)).map(item => item.toString(16).padStart(2, '0')).join('')
}

function stringToUint8(string) {
  const hexs = new Array()
  for (let i = 0; i < string.length; i += 2) {
    hexs.push(string.substring(i, i + 2))
  }
  return new Uint8Array(hexs.map(h => parseInt(h, 16)))
}

async function encrypt(message, key, salt) {
  const iv = (new TextEncoder()).encode(salt)
  const enc = await window.crypto.subtle.encrypt({name: 'aes-gcm', iv: iv}, key, (new TextEncoder()).encode(message))
  return uint8ToString(new Uint8Array(enc))
}

async function decrypt(message, key, salt) {
  const iv = (new TextEncoder()).encode(salt)
  const dec = await window.crypto.subtle.decrypt({name: 'aes-gcm', iv: iv}, key, stringToUint8(message))
  return (new TextDecoder()).decode(new Uint8Array(dec))
}

async function hash(string) {
  const hashArray = await window.crypto.subtle.digest('sha-256', (new TextEncoder()).encode(string))
  const hash = uint8ToString(new Uint8Array(hashArray))
  return hash
}

async function validate(theHash, parent) {
  const answer = document.createElement('input')
  answer.type = 'text'
  answer.addEventListener('input', () => {
    button.classList.remove('ctaError')
    button.classList.remove('ctaSuccess')
    button.value = "Vérifier"
  })
  const button = document.createElement('input')
  button.type = 'submit'
  button.className = 'ctaButton'
  button.value = 'Vérifier'
  button.addEventListener('click', async () => {
    const h = await hash(answer.value)
    if (h === theHash) {
      button.classList.add("ctaSuccess")
      button.value = "Nice!"
    } else {
      button.classList.add('ctaError')
      button.value = "Nope"
    }
  })
  parent.appendChild(answer)
  parent.appendChild(button)
}
